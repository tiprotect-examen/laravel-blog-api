<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title'   => 'required|min:5',
            'author'  => 'required|min:4',
            'content' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'El campo title es obligatorio',
            'title.min'      => 'El campo title debe tener almenos :min',

            'author.required' => 'El campo author es obligatorio',
            'author.min'      => 'El campo author debe tener almenos :min',

            'content.required' => 'El campo content es obligatorio'
        ];
    }
}
