<?php

namespace App\Http\Resources\Post;

use App\Models\Post;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PostCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($post) {
                return [
                    'id'         => $post->id,
                    'title'      => $post->title,
                    'author'     => $post->author,
                    'content'    => substr($post->content, 0, 70),
                    'created_at' => $post->created_at->format('Y-m-d H:i:s')
                ];
            })
        ];
    }
}
