<?php

namespace App\Http\Controllers;

use App\Http\Requests\Post\PostRequest;
use App\Http\Resources\Post\PostCollection;
use App\Http\Resources\Post\PostResource;
use App\Models\Post;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function index()
    {
        $posts = Post::query();
        if (\request()->filled('q')) {
            $q     = '%' . \request()->get('q') . '%';
            $posts = $posts->whereRaw('title LIKE ? COLLATE NOCASE', $q)
                ->orWhereRaw('author LIKE ? COLLATE NOCASE', $q)
                ->orWhereRaw('content LIKE ? COLLATE NOCASE', $q);
        }

        return new PostCollection($posts->orderByDesc('created_at')->paginate());
    }

    public function store(PostRequest $request)
    {
        $post = Post::create($request->validated());

        return new PostResource($post);
    }

    public function show($id)
    {
        try {
            $post = Post::findOrFail($id);
            return new PostResource($post);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'Este post no existe'], 404);
        }
    }
}
