## Instalar dependencias

````
composer install
````

## Ejecutar las migraciones con los seeders
````
php artisan migrate --seed --seeder=PostSeeder
````

## Iniciar servidor local
````
php artisan serve
````
